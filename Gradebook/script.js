"use strict";

const tableBody = document.getElementById("tableBody");
const nameInput = document.getElementById("nameInput");
const pointsEarned = document.getElementById("pointsEarned");
const pointsPossible = document.getElementById("pointsPossible");
const addButton = document.getElementById("addButton");
const homePage = document.getElementById("homePage");
const gradeBook = document.getElementById("gradeBook");

// addButton.addEventListener("click", addRecord);
nameInput.addEventListener("input", validateInputs);
pointsEarned.addEventListener("input", validateInputs);
pointsPossible.addEventListener("input", validateInputs);

// registering the click event to the add button - calling the addRecord function
addButton.addEventListener("click", addRecord);

//object array
let gradeArray = [];

/**
 * Verifies that inputs both have values
 * If so, enables (removes the disabled attr) the add button,
 * otherwise, it disables the button
 */
function validateInputs() {
    if (nameInput.value && pointsEarned.value && pointsPossible.value) {
        addButton.removeAttribute("disabled");
    } else {
        addButton.setAttribute("disabled", "");
    }
}

/**
 * appends record to the array, then calls the renderTableBody function
 * increments id
 */
function addRecord() {
    gradeArray.push({
        name: nameInput.value,
        pointsEarned: parseFloat(pointsEarned.value),
        pointsPossible: parseFloat(pointsPossible.value)
    });
    // set local storage here
    setStorage(gradeArray);
    renderTableBody();
    nameInput.value = "";
    pointsEarned.value = "";
    pointsPossible.value = "";
    validateInputs();
}

/**
 * Adds a record to the body of the table
 * REQUIRES name and description inputs to have a value
 * Clears out inputs after success
 * calculates percentages for gradebook
 * sets innerHTML of calculated grades
 */
function renderTableBody() {
    //clear out the table before we add all of the records to it
    tableBody.innerHTML = "";
    document.getElementById("letterGrade").innerHTML = "";
    document.getElementById("percentage").innerHTML = "";

    //variable creation for calculations
    let totalPoints = 0;
    let totalPossiblePoints = 0;
    let percentVar = 0;

    gradeArray.forEach((grade, index) => {
        //table creation
        const tableRow = tableBody.insertRow(-1);
        const nameCell = tableRow.insertCell(0);
        const pointsCell = tableRow.insertCell(1);
        const totalpointsCell = tableRow.insertCell(2);
        const deleteCell = tableRow.insertCell(3);

        //inner html for name and point
        nameCell.innerText = grade.name;
        pointsCell.innerText = grade.pointsEarned;
        //inner html of total points
        totalpointsCell.innerText = grade.pointsPossible;

        // variable population and calculation of the percentage
        totalPoints += grade.pointsEarned;
        totalPossiblePoints += grade.pointsPossible;
        percentVar = ((totalPoints / totalPossiblePoints) * 100).toFixed(2);

        // delete cell button creation
        deleteCell.innerHTML = `<button onclick="delAssignment(${index})">Delete Record</i></button>`;
    });
    //setting inner html for percentage
    document.getElementById("percentage").innerHTML = percentVar;
    const letterG = document.getElementById("letterGrade");

    // switch statement changes the html for letterGrade
    switch (true) {
        case percentVar >= 93:
            letterG.innerHTML = "A";
            break;
        case percentVar <= 93 && percentVar >= 90:
            letterG.innerHTML = "A-";
            break;
        case percentVar <= 90 && percentVar >= 87:
            letterG.innerHTML = "B+";
            break;
        case percentVar <= 87 && percentVar >= 83:
            letterG.innerHTML = "B";
            break;
        case percentVar <= 83 && percentVar >= 80:
            letterG.innerHTML = "B-";
            break;
        case percentVar <= 83 && percentVar >= 80:
            letterG.innerHTML = "B-";
            break;
        case percentVar <= 80 && percentVar >= 77:
            letterG.innerHTML = "C+";
            break;
        case percentVar <= 77 && percentVar >= 73:
            letterG.innerHTML = "C";
            break;
        case percentVar <= 73 && percentVar >= 70:
            letterG.innerHTML = "C-";
            break;
        case percentVar <= 70:
            letterG.innerHTML = "F";
            break;
        default:
            letterG.innerHTML = "Z";
    }
}
/**
 *
 * @param {Index} aIndex
 * when called deletes the Object at the index passed
 */

function delAssignment(aIndex) {
    if (confirm("Are you sure you want to delete the assignment?")) {
        gradeArray.splice(aIndex, 1);

        setStorage(gradeArray);
        renderTableBody();
    }
}
/**
 * toggle class toggles the class between homePage and GradeBook when called
 */

function toggleClass() {
    gradeBook.classList.toggle("removePage");
    homePage.classList.toggle("removePage");
}

// run on page load and attempts to retrieve data from local storage
// then sets the assignment array to the found data (if present)
function getDataFromStorage() {
    let storageData = window.localStorage.getItem("grades");
    if (storageData) {
        try {
            gradeArray = JSON.parse(storageData);

            // since I used an ID field w/ an incrementing integer, I need to update the id/counter to
            // be one more than the latest entry.  If you use the array index/splice method of delete, don't
            // worry about this
            // idValue =
            //     Math.max.apply(
            //         Math,
            //         gradeArray.map((myAssignment) => myAssignment.id)
            //     ) + 1;

            renderTableBody();
        } catch (e) {
            console.log(e);
            gradeArray = [];
        }
    }
}

/**
 *
 * @param {array of assignments} assignments
 */
function setStorage(assignments) {
    try {
        window.localStorage.setItem("grades", JSON.stringify(assignments));
    } catch (e) {
        console.log(e);
    }
}
// on page load, check local storage and render table if available
getDataFromStorage();
