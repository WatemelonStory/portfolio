import React, { useState, useEffect } from "react";

import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";


/**
 * 
 * @param {*} props 
 */
function PokemonAdd(props) {
    const [nameVariable, setNameVariable] = useState("");
    const [typeVariable, setTypeVariable] = useState("");
    const [descriptionVariable, setDescriptionVariable] = useState("");
    const [imageLinkVariable, setImageLinkVariable] = useState("");
    const [disabledButton, setDisabledButton] = useState(true);

    /**
     * Executes on load and with state change
     */
    useEffect(() => {
        validateInputs();
    });

    function validateInputs() {
        if (nameVariable && typeVariable && descriptionVariable && imageLinkVariable) {
            setDisabledButton(false);
        } else {
            setDisabledButton(true);
        }
    }

    async function postPokemon() {
        try {
            const response = await axios({
                method: "post", //you can set what request you want to be: delete, get, post
                url: "https://web-app-pokemon.herokuapp.com/pokemon",
                data: {
                    name: nameVariable,
                    type: typeVariable,
                    description: descriptionVariable,
                    image: imageLinkVariable
                }, //only include data on post/put
                headers: {
                    "User-Id": "BuyMe",
                    "Content-Type": "application/json"
                }
            });
            //do something on success
            const responseBody = response.data;
        } catch (e) {
            //some error occurred
        }
    }

    return (
        <div>
            <div>
                <TextField
                    id="standard-basic"
                    label="Name"
                    placeholder="Add Pokemon Name"
                    onChange={(event) => setNameVariable(event.target.value)}
                />

                <TextField
                    id="standard-basic"
                    label="Type"
                    placeholder="Add Pokemon Type"
                    onChange={(event) => setTypeVariable(event.target.value)}
                />

                <TextField
                    id="standard-basic"
                    label="Desc"
                    placeholder="Add Pokemon Description"
                    onChange={(event) => setDescriptionVariable(event.target.value)}
                />

                <TextField
                    id="standard-basic"
                    label="Image"
                    placeholder="Add Pokemon Image"
                    onChange={(event) => setImageLinkVariable(event.target.value)}
                />
            </div>
            <div>
                <Button
                    variant="contained"
                    color="primary"
                    disabled={disabledButton}
                    onClick={() => {
                        postPokemon();
                        props.setCurrentComponent("list");
                    }}
                >
                    Add Pokemon
                </Button>
            </div>
        </div>
    );
}

export default PokemonAdd;
