import React, { useState, useEffect } from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { makeStyles } from "@material-ui/core/styles";
import axios from "axios";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";

import ViewHeadlineIcon from "@material-ui/icons/ViewHeadline";
import Button from "@material-ui/core/Button";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";

// Alert function for SnackBars
function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

/**
 * useStyles for styleing
 */
const useStyles = makeStyles((theme) => ({
    tableStyle: {
        marginLeft: "auto",
        marginRight: "auto",
        width: "60%"
    },
    menuButton: {
        marginRight: theme.spacing(2)
    },
    title: {
        flexGrow: 1
    },
    appBar: {
        marginBottom: theme.spacing(2)
    },
    table: {
        width: "80%",
        margin: "auto"
    },
    head: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.secondary.main
    }
}));


/**
 * List Function displays Table of pokemon
 * Deletes pokemon
 * @param {*} props 
 */
function List(props) {
    const classes = useStyles();
    const [pokemon, setPokemon] = useState([]);
    const [snackbar, setSnackbar] = useState(false);

    //closes snackbars
    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setSnackbar(false);
    };

    /**
     * useffect is run at load and component change
     */
    useEffect(() => {
        listPokemon();
    }, []);
    /**
     * Get API call for pokemon
     */
    async function listPokemon() {
        try {
            const response = await axios({
                method: "get",
                url: "https://web-app-pokemon.herokuapp.com/pokemon",
                headers: {
                    "User-Id": "BuyMe"
                }
            });
            console.log(response.data);
            setPokemon(response.data);
        } catch (e) {
            console.log(e);
        }
    }
    /**
     *  Deletes pokemon
     * @param {ID of pokemon you want to delete} deleteId 
     */
    async function deletePokemon(deleteId) {
        try {
            const response = await axios({
                method: "delete",
                url: `https://web-app-pokemon.herokuapp.com/pokemon/${deleteId}`,
                headers: {
                    "User-Id": "BuyMe"
                }
            });
            listPokemon();
            console.log(response.data);
        } catch (e) {
            setSnackbar(true);
        }
    }

    return (
        <div>
            <Snackbar open={snackbar} autoHideDuration={3000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="warning">
                    This Pokemon is an example that can not be deleted
                </Alert>
            </Snackbar>
            <TableContainer className={classes.table}>
                <Table className={classes.table}>
                    <TableHead className={classes.head}>
                        <TableRow>
                            <TableCell>Name</TableCell>
                            <TableCell>Type</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody className={classes.tableBody}>
                        {pokemon.map((currentMon) => (
                            <TableRow key={currentMon.id}>
                                <TableCell>{currentMon.name}</TableCell>
                                <TableCell>{currentMon.type}</TableCell>
                                <TableCell>
                                    <HighlightOffIcon color="primary" onClick={() => deletePokemon(currentMon.id)} />
                                    <ViewHeadlineIcon
                                        color="secondary"
                                        onClick={() => {
                                            props.setCurrentComponent("view");
                                            props.setPokemonId(currentMon.id);
                                        }}
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
            <Button variant="outlined" color="primary" onClick={() => props.setCurrentComponent("add")}>
                Add Pokemon
            </Button>
        </div>
    );
}
export default List;
