import { useState } from "react";
import "./App.css";
import Image from "./images/fd18c6d26d4d9d26a0bd9d1a2fb2bd04.png";
import List from "./components/List";
import PokemonAdd from "./components/PokemonAdd";
import PokemonView from "./components/PokemonView";
import { makeStyles } from "@material-ui/core/styles";

/**
 * styling for main page
 */
const useStyles = makeStyles((theme) => ({
    Img: {
        width: "60%",
        height: "auto"
    },
    Class: {
        textAlign: "center",
        margin: "auto"
    }
}));

/**
 * displaying of pages
 */
function App() {
    const classes = useStyles();
    const [currentComponent, setCurrentComponent] = useState("list");
    const [pokemonId, setPokemonId] = useState("");
    return (
        <div className={classes.Class}>
            {<img src={Image} alt="Pokemon" className={classes.Img} />}
            {currentComponent === "list" && (
                <List setCurrentComponent={setCurrentComponent} setPokemonId={setPokemonId} />
            )}
            {currentComponent === "view" && <PokemonView setCurrentComponent={setCurrentComponent} id={pokemonId} />}
            {currentComponent === "add" && <PokemonAdd setCurrentComponent={setCurrentComponent} />}
        </div>
    );
}
export default App;
